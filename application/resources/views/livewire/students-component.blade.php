<div class="overflow-auto">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Manage Students
                        <button class="btn btn-primary" style="float: right" data-bs-toggle="modal" data-bs-target="#studentModal">Add Students</button>
                    </div>

                    <div class="card-body">
                        @if (session()->has('message'))
                            <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('message') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        @endif

                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th style="text-align: center;">Options</th>
                                    </tr>
                            </thead>

                            <tbody>
                                @if ($students->count() > 0)
                                    @foreach ($students as $student)
                                        <tr>
                                            <td>{{ $student->student_id }}</td>
                                            <td>{{ $student->name }}</td>
                                            <td>{{ $student->email }}</td>
                                            <td>{{ $student->phone }}</td>
                                            <td>{{ $student->address }}</td>
                                            <td style="text-align: center;">
                                                <center>
                                                    <button class="btn btn-sm btn-secondary" wire:click="viewStudentDetails({{ $student->id }} )">View</button>
                                                    <button class="btn btn-sm btn-primary" wire:click="editStudents({{ $student->id }} )">Edit</button>
                                                    <button class="btn btn-sm btn-danger" wire:click="deleteConfirmation({{ $student->id }} )">Delete</button>
                                                </center>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" style="text-align: center;"><small>No Student Found</small></td>
                                    </tr>
                                @endif
                            </tbody>
  
                        </table>
                
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="studentModal" tabindex="-1" aria-labelledby="studentModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="studentModalLabel">Add Student</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <form wire:submit.prevent="storedata">
                        <div class="mb-3 row">
                            <label for="studentID" class="col-sm-3 col-form-label">Student ID</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="student_id" wire:model="student_id">
                                @error('student_id')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="studentName" class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" wire:model="name">
                                @error('name')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="studentEmail" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" wire:model="email">
                                @error('email')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="studentMobile" class="col-sm-3 col-form-label">Mobile No</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="phone" wire:model="phone">
                                @error('phone')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="studentAddress" class="col-sm-3 col-form-label">Address</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="address" wire:model="address">
                                @error('address')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3 row">
                            <label for="button" class="col-sm-3"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-sm btn-primary">Add Student</button>
                            </div>
                        </div>

                    </form>
                </div>

            
            </div>
        </div>
    </div>

    <!-- editModal -->
    <div wire:ignore.self class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="editModalLabel">Edit Student</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" wire:click="close"></button>
                </div>

                <div class="modal-body">
                    <form wire:submit.prevent="editStudentData">
                        <div class="mb-3 row">
                            <label for="studentID" class="col-sm-3 col-form-label">Student ID</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="student_id" wire:model="student_id">
                                @error('student_id')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="studentName" class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" wire:model="name">
                                @error('name')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="studentEmail" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" wire:model="email">
                                @error('email')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="studentMobile" class="col-sm-3 col-form-label">Mobile No</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="phone" wire:model="phone">
                                @error('phone')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="studentAddress" class="col-sm-3 col-form-label">Address</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="address" wire:model="address">
                                @error('address')
                                    <span class="text-danger" style="font-size: 11.5px;">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3 row">
                            <label for="button" class="col-sm-3"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-sm btn-primary" wire:click="">Edit Student</button>
                            </div>
                        </div>

                    </form>
                </div>

            
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="deleteModalLabel">Delete Confirmation</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="pt-4 pb-4 modal-body">
                    <h6>Are you sure? You want to delete this student data!</h6>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" wire:click="cancel()" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                    <button class="btn btn-sm btn-danger" wire:click="deleteData()">Yes! Delete</button>
                </div>

            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="viewModal" tabindex="-1" aria-labelledby="viewModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="viewModalLabel">Student Information</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" wire:click="closeViewStudentModal"></button>
                </div>

                <div class="modal-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>ID: </th>
                                <TD>{{ $view_student_id }}</td>
                            </tr>

                            <tr>
                                <th>Name: </th>
                                <TD>{{ $view_student_name }}</td>
                            </tr>
                            <tr>
                                <th>Email: </th>
                                <TD>{{ $view_student_email }}</td>
                            </tr>
                            <tr>
                                <th>Phone: </th>
                                <TD>{{ $view_student_phone }}</td>
                            </tr>
                            <tr>
                                <th>Address: </th>
                                <TD>{{ $view_student_address }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            
            </div>
        </div>
    </div>


</div>

@push('scripts')
    <script>
        window.addEventListener('close-modal', event =>{
            $('#studentModal').modal('hide');
            $('#editModal').modal('hide');
            $('#deleteModal').modal('hide');
        });
        
        window.addEventListener('show-edit-student-modal', event =>{
            $('#editModal').modal('show');
        });

        window.addEventListener('show-delete-confirmation-modal', event =>{
            $('#deleteModal').modal('show');
        })
               
        window.addEventListener('show-view-student-modal', event =>{
            $('#viewModal').modal('show');
        })

    </script>
@endpush